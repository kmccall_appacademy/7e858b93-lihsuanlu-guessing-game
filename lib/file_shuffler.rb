puts "what file do you want to be shuffled"
filename = gets.chomp
linez = File.readlines(filename)
total_lines = linez.length
shuf_amt = rand(total_lines-1)+1
File.open(filename[0..-5]+'-shuffled.txt',"w") do |f|
  (0...total_lines).each do |line|
    f.puts linez[(line+shuf_amt)%total_lines]
  end
end
