# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
  correct_num = rand(100)+1
  tries = 0
  guess = correct_num-1
  while guess != correct_num
    if guess > correct_num
      puts "too high"
    else
      puts "too low"
    end
    puts "guess a number between 1 and 100"
    guess = gets.chomp.to_i
    puts guess.to_s
    tries += 1
  end
  puts "you guessed it, the number was " + correct_num.to_s
  puts "and you took " + tries.to_s + " tries"
end
